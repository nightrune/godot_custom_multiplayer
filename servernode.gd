extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var SERVER_PORT = 9000
var MAX_PLAYERS = 2
var api = null

# Called when the node enters the scene tree for the first time.
func _ready():
	api = MultiplayerAPI.new()
	var peer = NetworkedMultiplayerENet.new()
	var error = peer.create_server(SERVER_PORT, MAX_PLAYERS)
	print(error)
	api.network_peer = peer
	api.set_root_node(self)
	self.set_custom_multiplayer(api)
	api.connect("network_peer_connected", self, "_player_connected")
	api.connect("network_peer_disconnected", self, "_player_disconnected")
	api.connect("connection_failed", self, "_connected_fail")
	api.connect("server_disconnected", self, "_server_disconnected")
	print(api)

func _player_connected(id):
	print("Player connected: " + String(id))
	rpc_id(id, "test")

func _player_disconnected(_id):
	print("Player Disconnected")

func _server_disconnected():
	print("Sever Disconnected")

func _connected_fail():
	print("Connection Failed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if api != null:
		api.poll()
