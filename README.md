# Implementing a Client and Server in one scene! #

## Goal ##
To make my workflow faster, I wanted to find a way to test a client and server
at the same time in godot. Many games with custom engines do this already so
I figured that godot could as well.

### Discovery ###

I came across this line in [MultiplayerAPI](https://docs.godotengine.org/en/stable/classes/class_multiplayerapi.html) documentation on the godot documentation.

> It is possible to override the MultiplayerAPI instance used by specific Nodes by setting the Node.custom_multiplayer property, effectively allowing to run both client and server in the same scene.

Great how exactly do you do this!

Well it took making this example to find out what all needed to happen.

## Implentation ##

[Client](clientnode.gd)

[Server](servernode.gd)

Lets take a closer look at the server script:

This node is at the root of a Node2D. I believe it can be anywhere! I have not
experimented with it though
```
api = MultiplayerAPI.new()
var peer = NetworkedMultiplayerENet.new()
var error = peer.create_server(SERVER_PORT, MAX_PLAYERS)
print(error)
api.network_peer = peer
api.set_root_node(self)
self.set_custom_multiplayer(api)
```

So instead of the normal
```
var peer = NetworkedMultiplayerENet.new()
var error = peer.create_server(SERVER_PORT, MAX_PLAYERS)
get_tree().network_peer = peer;
```
We instead create our own api object. Set the currents nodes custom_multiplayer
to that api object. Set the root node of the api to the same node we set
custom_multiplayer on, and set the network_peer of the api to our newly created
peer.

The key to making it work is this though:
```
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if api != null:
		api.poll()
```
Turns out the SceneTree already calls this for us if you use the standard method,
but I find this workflow will be simpler as I can now create client, and server
scenes and just instantiate them in the main scene I need.

Hopefully this is useful for others!