extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var SERVER_IP = "127.0.0.1"
var SERVER_PORT = 9000
var api = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func connect_to_server():
	if api != null:
		print("Already created client")
		return
	print("Attempting to connect to server")
	api = MultiplayerAPI.new()
	var peer = NetworkedMultiplayerENet.new()
	var error = peer.create_client(SERVER_IP, SERVER_PORT)
	print(error)
	api.network_peer = peer
	self.set_custom_multiplayer(api)
	api.set_root_node(self)
	api.connect("connected_to_server", self, "_connected_ok")
	api.connect("connection_failed", self, "_connected_fail")
	api.connect("server_disconnected", self, "_server_disconnected")
	print("Should be connecting..")
	print(api)

func _connected_ok():
	print("Connected OK!")

func _server_disconnected():
	print("Sever Disconnected")

func _connected_fail():
	print("Connection Failed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if api != null:
		api.poll()
		
remote func test():
	print("Called from server")
